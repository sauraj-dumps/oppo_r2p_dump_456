## awaken_r2p-user 11 RQ3A.210905.001 eng.cirrus.20210926.232733 release-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: r2p
- Brand: OPPO
- Flavor: awaken_r2p-user
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.cirrus.20210926.232733
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OPPO/awaken_r2p/r2p:11/RQ3A.210905.001/cirrus09262324:user/release-keys
- OTA version: 
- Branch: awaken_r2p-user-11-RQ3A.210905.001-eng.cirrus.20210926.232733-release-keys
- Repo: oppo_r2p_dump_456


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
